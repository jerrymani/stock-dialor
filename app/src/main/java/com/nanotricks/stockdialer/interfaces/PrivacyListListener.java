package com.nanotricks.stockdialer.interfaces;

public interface PrivacyListListener {
    void onContactChecked(String number);
}
