package com.nanotricks.stockdialer.interfaces;

import com.nanotricks.stockdialer.models.FragmentInteraction;

public interface OnFragmentInteractionListener {
    void onFragmentInteraction(FragmentInteraction fragmentInteraction);
}
