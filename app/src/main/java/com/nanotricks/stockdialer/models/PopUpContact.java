package com.nanotricks.stockdialer.models;

public class PopUpContact {
    private String name;
    private String phoneActive;
    private String missedCall;
    private String inComingCall;
    private String outGoingCall;
    private String imageUri;

    public PopUpContact(String name, String phoneActive, String missedCall, String inComingCall, String outGoingCall, String imageUri) {
        this.name = name;
        this.phoneActive = phoneActive;
        this.missedCall = missedCall;
        this.inComingCall = inComingCall;
        this.outGoingCall = outGoingCall;
        this.imageUri = imageUri;
    }

    public PopUpContact() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneActive() {
        return phoneActive;
    }

    public void setPhoneActive(String phoneActive) {
        this.phoneActive = phoneActive;
    }

    public String getMissedCall() {
        return missedCall;
    }

    public void setMissedCall(String missedCall) {
        this.missedCall = missedCall;
    }

    public String getInComingCall() {
        return inComingCall;
    }

    public void setInComingCall(String inComingCall) {
        this.inComingCall = inComingCall;
    }

    public String getOutGoingCall() {
        return outGoingCall;
    }

    public void setOutGoingCall(String outGoingCall) {
        this.outGoingCall = outGoingCall;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }
}
