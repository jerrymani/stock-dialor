package com.nanotricks.stockdialer.models;

import org.json.JSONObject;

public class FragmentInteraction {
    private String action;
    private JSONObject data;

    public FragmentInteraction(String action, JSONObject data) {
        this.action = action;
        this.data = data;
    }

    public FragmentInteraction() {
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }
}
