package com.nanotricks.stockdialer.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.nanotricks.stockdialer.models.CustomDialogOptions;

public class CustomDialogFragment extends DialogFragment {
    private final static CustomDialogFragment instance = new CustomDialogFragment();
    private static OnDialogFragmentClickListener listener;
    private static CustomDialogOptions options;

    public static CustomDialogFragment getInstance(CustomDialogOptions _options,OnDialogFragmentClickListener _listener) {
        options = _options;
        listener = _listener;
        return instance;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(options == null) {
            options = new CustomDialogOptions();
        }
        String title = options.getTitle();
        String message = options.getMessage();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(options.getPositiveButtonText(),  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(listener!=null) {
                    listener.onDialogOkClicked();
                }
            }
        });
        alertDialogBuilder.setNegativeButton(options.getNegativeButtonText(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    if(listener!=null) {
                        listener.onDialogCancelClicked();
                    } else {
                        dialog.dismiss();
                    }
                }
            }
        });
        return alertDialogBuilder.create();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commit();
        } catch (IllegalStateException e) {
            Log.e("ACE:IllegalState", "Exception", e);
        }
    }

    public interface OnDialogFragmentClickListener {
        void onDialogOkClicked();

        void onDialogCancelClicked();
    }
}
