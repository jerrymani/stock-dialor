package com.nanotricks.stockdialer;

import android.content.Context;

import com.nanotricks.stockdialer.utils.Pref;

public class Util {

    private final static Util util = new Util();

    public static synchronized Util getInstance() {
        return util;
    }

    Pref getPreference(Context context) {
        return new Pref(context);
    }

}
