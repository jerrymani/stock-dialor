package com.nanotricks.stockdialer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nanotricks.stockdialer.R;
import com.nanotricks.stockdialer.interfaces.ContactListLisenter;
import com.nanotricks.stockdialer.models.ContactListModel;

import java.util.List;

public class ContactListAdapter  extends RecyclerView.Adapter<ContactListAdapter.ContactViewHolder>{

    private List<ContactListModel> contactList;
    private Context mContext;
    private String TAG = "ContactListAdapter";
    private ContactListLisenter lisenter;

    public ContactListAdapter(List<ContactListModel> contactList, Context mContext,ContactListLisenter lisenter) {
        this.contactList = contactList;
        this.mContext = mContext;
        this.lisenter = lisenter;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_view_contact_list, viewGroup,false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder contactViewHolder, int i) {
        final ContactListModel contact = contactList.get(i);
        Log.i(TAG, "onBindViewHolder: "+contact);
//        contactViewHolder.tvCallLogText.setText(contact.getLastLog());
        contactViewHolder.tvPhoneNumber.setText(contact.getPhoneNumber1());
        contactViewHolder.tvContactName.setText(contact.getName());
        contactViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(lisenter!=null) {
                    lisenter.onContactClick(contact.getPhoneNumber1());
                }
            }
        });
        contactViewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(lisenter!=null) {
                    lisenter.onContactLongClick(contact.getPhoneNumber1());
                    return true;
                }
                return false;
            }
        });
        contactViewHolder.ivContactImage.setImageResource(R.drawable.boy);
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder{

        ImageView ivContactImage;
        TextView tvContactName;
        TextView tvPhoneNumber;
        ImageView ivCallLogImage;
        TextView tvCallLogText;

        public ContactViewHolder(View itemView) {
            super(itemView);
            ivContactImage = itemView.findViewById(R.id.contact_image_iv);
            tvContactName  = (TextView)itemView.findViewById(R.id.contact_name_tv);
            tvPhoneNumber = (TextView)itemView.findViewById(R.id.phone1_tv);
            ivCallLogImage = itemView.findViewById(R.id.contact_log_iv);
            tvCallLogText = (TextView)itemView.findViewById(R.id.contact_log_tv);
        }
    }
}
