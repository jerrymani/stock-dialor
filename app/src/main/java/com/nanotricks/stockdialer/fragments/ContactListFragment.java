package com.nanotricks.stockdialer.fragments;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nanotricks.stockdialer.R;
import com.nanotricks.stockdialer.Util;
import com.nanotricks.stockdialer.adapter.ContactListAdapter;
import com.nanotricks.stockdialer.interfaces.ContactListLisenter;
import com.nanotricks.stockdialer.interfaces.OnFragmentInteractionListener;
import com.nanotricks.stockdialer.models.ContactListModel;
import com.nanotricks.stockdialer.models.PopUpContact;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class ContactListFragment extends Fragment implements ContactListLisenter {

    private OnFragmentInteractionListener mListener;
    private View view;
    private RecyclerView contactRecyclerView;
    private Toolbar toolbar;
    List<ContactListModel> contactVOList = new ArrayList();
    public ContactListAdapter contactListAdapter;
    private Context mContext;
    private String TAG = "ContactList";
    private Dialog myDialog;

    public ContactListFragment() {
        // Required empty public constructor
    }

    public static ContactListFragment newInstance() {
        return new ContactListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_contact_list, container, false);
        bindViews();
        doOperation();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void doOperation() {
        setupToolBar();
        setupRecyclerView();
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_CONTACTS)
                == PackageManager.PERMISSION_GRANTED) {

            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    getAllContacts();
                }
            });
        }
    }

    private void setupRecyclerView() {
        contactListAdapter = new ContactListAdapter(contactVOList,getActivity(),this);
        contactRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        contactRecyclerView.setAdapter(contactListAdapter);
    }
    private void getAllContacts() {
        ContactListModel contactVO;
        ContentResolver contentResolver = Objects.requireNonNull(getActivity()).getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0) {
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    contactVO = new ContactListModel();
                    contactVO.setName(name);

                    Cursor phoneCursor = contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id},
                            null);
                    assert phoneCursor != null;
                    if (phoneCursor.moveToNext()) {
                        String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        contactVO.setPhoneNumber1(phoneNumber);
                    }

//                    contactVO.setLastLogType(phoneCursor.getString(phoneCursor.getColumnIndex(CallLog.Calls.TYPE)));
                    String lastTime = "";
                    int lastTimeUsed = phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.LAST_TIME_USED);
                    if(phoneCursor.getCount() > 0 && lastTimeUsed > 0 &&
                            phoneCursor.getLong(lastTimeUsed) > 0) {
                        lastTime = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(
                                phoneCursor.getLong(lastTimeUsed));
                    }
                    contactVO.setLastLog(lastTime);
                    phoneCursor.close();
                    contactVOList.add(contactVO);
                }
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    contactListAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void onContactClick(String number) {
        //show popup
        Log.i(TAG, "onContactClick: number - "+number);
        PopUpContact contact = new PopUpContact("name",number,"100","50","10","uri");
        showPopup(contact);
    }

    @Override
    public void onContactLongClick(String number) {
        Log.i(TAG, "onContactLongClick: number-"+number);
    }

    public void showPopup(PopUpContact data) {
        TextView txtclose, tvMissed, tvInComimg, tvOutGoing, tvName, tvPhoneActive;
        Button btnCall;
        myDialog.setContentView(R.layout.popup_dialog);
        txtclose = (TextView) myDialog.findViewById(R.id.txtclose);

        tvPhoneActive =(TextView) myDialog.findViewById(R.id.popup_active);
        tvInComimg =(TextView) myDialog.findViewById(R.id.popup_incoming);
        tvOutGoing =(TextView) myDialog.findViewById(R.id.popup_outgoing);
        tvMissed =(TextView) myDialog.findViewById(R.id.popup_missed);
        tvName =(TextView) myDialog.findViewById(R.id.popup_name);

        tvInComimg.setText(data.getInComingCall());
        tvMissed.setText(data.getMissedCall());
        tvOutGoing.setText(data.getOutGoingCall());
        tvName.setText(data.getName());
        tvPhoneActive.setText(data.getPhoneActive());

        btnCall = (Button) myDialog.findViewById(R.id.btnfollow);
        txtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }

    private void setupToolBar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        toolbar.setTitle("Name");
        toolbar.setSubtitle("8754058922");
        toolbar.setLogo(R.drawable.profile_man_64);
    }

    private void bindViews() {
        contactRecyclerView =  view.findViewById(R.id.contact_lost_rv);
        toolbar = view.findViewById(R.id.contact_list_tb);
        myDialog = new Dialog(getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search_bar:

                // Do Activity menu item stuff here
                return true;

            case R.id.privacy:

                // Not implemented here
                return true;
            default:
                break;
        }

        return false;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
