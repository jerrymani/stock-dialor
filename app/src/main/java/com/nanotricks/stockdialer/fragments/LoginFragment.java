package com.nanotricks.stockdialer.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.nanotricks.stockdialer.R;
import com.nanotricks.stockdialer.interfaces.OnFragmentInteractionListener;
import com.nanotricks.stockdialer.models.FragmentInteraction;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginFragment extends Fragment {
    private OnFragmentInteractionListener mListener;
    private View view;
    private EditText phone1, phone2, name;
    private Button login;
    private JSONObject dataIntent = new JSONObject();

    public LoginFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_login, container, false);
        bindViews();
        doOperations();
        return view;
    }

    private void doOperations() {
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSuccessLogin();
            }
        });
    }

    private void onSuccessLogin() {
        if(mListener!=null) {
            try {
                dataIntent.put("isLogged", true);
                dataIntent.put("name", "Arul");
                dataIntent.put("phone1", "8754058922");
                dataIntent.put("phone2", "-");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mListener.onFragmentInteraction(new FragmentInteraction("login",dataIntent));
        }
    }

    private void bindViews() {
        login = view.findViewById(R.id.bt_login);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
