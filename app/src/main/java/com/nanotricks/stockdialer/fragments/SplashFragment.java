package com.nanotricks.stockdialer.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nanotricks.stockdialer.R;
import com.nanotricks.stockdialer.interfaces.OnFragmentInteractionListener;
import com.nanotricks.stockdialer.models.FragmentInteraction;

import org.json.JSONException;
import org.json.JSONObject;

public class SplashFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private JSONObject dataIntent =new JSONObject();
    private String TAG = "Splash";
    public SplashFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static SplashFragment newInstance() {
        return new SplashFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                onSplashCompleted();
            }
        }, 3000);

        return inflater.inflate(R.layout.fragment_splash, container, false);
    }
    public void onSplashCompleted() {
        Log.i(TAG, "onSplashCompleted");
        if (mListener != null) {
            Log.i(TAG, "login");
            try {
                dataIntent.put("splashCompleted",true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mListener.onFragmentInteraction(new FragmentInteraction("splash",dataIntent));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


}
