package com.nanotricks.stockdialer.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nanotricks.stockdialer.R;
import com.nanotricks.stockdialer.interfaces.OnFragmentInteractionListener;

public class CallLogFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private View view;

    public CallLogFragment() {
        // Required empty public constructor
    }


    public static CallLogFragment newInstance() {
        return new CallLogFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_call_log, container, false);
        bindViews();
        doOperation();
        return view;
    }

    private void doOperation() {

    }

    private void bindViews() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
